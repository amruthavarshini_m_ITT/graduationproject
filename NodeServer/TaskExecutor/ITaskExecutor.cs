﻿using NodeApplication.CommunicationProtocol;
using System;
using System.Collections.Generic;
using System.Text;

namespace NodeApplication.TaskExecutor
{
    public interface ITaskExecutor
    {
        TDSResult ExecuteTask(TaskModel task);
    }
}
