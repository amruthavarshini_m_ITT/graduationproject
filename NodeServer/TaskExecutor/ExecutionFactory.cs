﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NodeApplication.TaskExecutor
{
    public class ExecutionFactory
    {
        public static ITaskExecutor GetTaskExector(string fileExtension)
        {
            return fileExtension switch
            {
               
                ".cs" => new CSharpTaskExecutor(),
                _ => null,
            };
        }
    }
}
