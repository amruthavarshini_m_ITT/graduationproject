﻿using NodeApplication.CommunicationProtocol;
using NodeApplication.CommunicationProtocol.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NodeApplication.TaskExecutor
{
    public class ExecutionHandler
    {
        public string GenerateExecutableFile(TaskModel task, string fileType)
        {
            try
            {
                string currentFileName = "task_" + task.TaskGuid + fileType;
                string taskExecutablesDirPath = Directory.GetCurrentDirectory() + @"\TaskExecutables\";
                Directory.CreateDirectory(taskExecutablesDirPath);
                string currentFilePath = taskExecutablesDirPath + currentFileName;
                File.Create(currentFilePath).Close();
                File.WriteAllBytes(currentFilePath, Convert.FromBase64String(task.TaskExecutable));
                return currentFilePath;
            }
            catch (Exception exception)
            {
                throw new Exception("Error in generating executable file: " + exception.Message);
            }
        }

        public TDSResult GetResult(TaskModel task, TaskStatus status, string errorCode, string errorMessage, string output)
        {
            return new TDSResult()
            {
                TaskGuid = task.TaskGuid,
                ErrorCode = errorCode,
                ErrorMessage = errorMessage,
                TaskStatus = status,
                TaskOutcome = output
            };
        }
    }
}
