﻿using NodeApplication.CommunicationProtocol;
using NodeApplication.CommunicationProtocol.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NodeApplication.TaskExecutor
{
    public class CSharpTaskExecutor : ITaskExecutor
    {
        private readonly string _cSharpExecutable = "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Professional\\MSBuild\\Current\\Bin\\Roslyn\\csc.exe";
        public TDSResult ExecuteTask(TaskModel task)
        {
            TDSResult result;
            ExecutionHandler handler = new ExecutionHandler();
            try
            {
                string output = string.Empty;
                

                string fileType = Path.GetExtension(task.TaskPath);
                string filePath = handler.GenerateExecutableFile(task, fileType);

                TaskProcessor processor = new TaskProcessor();

                output = processor.RunTask(_cSharpExecutable, filePath);  // C# Compilation

                string fileName = Path.GetFileNameWithoutExtension(filePath) + ".exe";

                if (File.Exists(fileName))
                {
                    output = processor.RunTask(fileName, string.Empty);  // C# execution
                    result = handler.GetResult(task, TaskStatus.COMPLETED, "200", "SUCCESS", output);
                }
                else
                {
                    output = "Compilation Error: " + output;
                    throw new InvalidOperationException(output);
                }
                File.Delete(filePath);
                File.Delete(fileName);
            }
            catch (InvalidOperationException ex)
            {
                result = handler.GetResult(task, TaskStatus.ABORTED, "400", "BAD REQUEST", ex.Message);
            }
            catch (Exception ex)
            {
                result = handler.GetResult(task, TaskStatus.ABORTED, "500", "INTERNAL SERVER ERROR", ex.Message);
            }

            return result;
        }
    }
}
