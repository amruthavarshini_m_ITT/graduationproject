﻿using Newtonsoft.Json;


namespace NodeApplication.CommunicationProtocol
{
    public class TDSSerializer : ITDSSerializer
    {
       

        public TaskModel DeSerializeTaskModel(string data)
        {
            return JsonConvert.DeserializeObject<TaskModel>(data);
        }

        public TDSResult DeSerializeTDSResult(string data)
        {
            return JsonConvert.DeserializeObject<TDSResult>(data);
        }


        public string SerializeTask(TaskModel task)
        {
            string json = JsonConvert.SerializeObject(task);
            return json;
        }

        public string SerializeTDSResult(TDSResult result)
        {
            string json = JsonConvert.SerializeObject(result);
            return json;
        }
    }
}
