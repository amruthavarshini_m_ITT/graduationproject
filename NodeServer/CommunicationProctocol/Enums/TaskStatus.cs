﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeApplication.CommunicationProtocol.Enums
{
    public enum TaskStatus
    {
        PENDING,
        QUEUED,
        EXECUTING,
        COMPLETED,
        ABORTED
    }
}
