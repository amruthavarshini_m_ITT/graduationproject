﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NodeApplication.CommunicationProtocol
{
    interface ITDSSerializer
    {
        //DeSerialize the String data into a TDSProtocol object
        

        TaskModel DeSerializeTaskModel(string data);

        TDSResult DeSerializeTDSResult(string data);

        //Serialize the TDSProtocol object to a String format
       

        string SerializeTask(TaskModel task);

        string SerializeTDSResult(TDSResult result);
    }
}
