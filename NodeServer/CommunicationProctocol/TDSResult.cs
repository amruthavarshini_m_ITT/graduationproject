﻿using NodeApplication.CommunicationProtocol.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeApplication.CommunicationProtocol
{
    public class TDSResult
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string TaskGuid { get; set; }
        public TaskStatus TaskStatus { get; set; }
        public string TaskOutcome { get; set; }
    }
}
