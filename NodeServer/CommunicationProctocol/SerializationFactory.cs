﻿using NodeApplication.CommunicationProtocol.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeApplication.CommunicationProtocol
{
    class SerializationFactory
    {
        public static ITDSSerializer GetSerializer(SerializerType type)
        {
           switch (type)
            {
                case SerializerType.JSON:
                    return new TDSSerializer();

                case SerializerType.XML:
                    return new TDSSerializer();

                default:
                    throw new InvalidOperationException("Invalid Serializer type");
            }
        }
    }
}
