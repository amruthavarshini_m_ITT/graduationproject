﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeApplication.CommunicationProtocol
{
    public class TaskModel
    {
        public int TaskID { get; set; }
        public string TaskPath { get; set; }
        public string TaskResult { get; set; }
        public string TaskParameters { get; set; }
        public int TaskStatusID { get; set; }
        public int ClientID { get; set; }
        public string TaskGuid { get; set; }
        public int TaskStatus { get; set; }

        public string TaskExecutable { get; set; }
    }
}
