﻿using NodeApplication.CommunicationLayer;
using System;
using System.Net;

namespace NodeApplication
{
    class NodeProgram
    {
        static void Main(string[] args)
        {
            IPAddress nodeServerIp = IPAddress.Parse("::1");
            int nodePort = 14000;
            NodeServer server = new NodeServer(nodeServerIp, nodePort);

            server.StartListening();
           // return 0;
        }
    }
}
