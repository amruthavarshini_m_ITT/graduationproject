﻿using NodeApplication.CommunicationProtocol;
using NodeApplication.CommunicationProtocol.Enums;
using NodeApplication.TaskExecutor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;


namespace NodeApplication.CommunicationLayer
{
    public class NodeServer
    {
        private readonly IPAddress _nodeServerIpAddress;
        private readonly int _nodePort;

        public NodeServer(IPAddress ipAddress, int port)
        {
            _nodeServerIpAddress = ipAddress;
            _nodePort = port;
        }

        public static ManualResetEvent allDone = new ManualResetEvent(false); // Thread signal.

        public void StartListening()
        {
            // Establish the local endpoint for the socket.  
            // The DNS name of the computer  
            // running the listener is "host.contoso.com".  
            IPEndPoint localEndPoint = new IPEndPoint(_nodeServerIpAddress, _nodePort);

            // Create a TCP/IP socket.  
            Socket listener = new Socket(_nodeServerIpAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.  
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    // Set the event to nonsignaled state.  
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.  
                    Console.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    // Wait until a connection is made before continuing.  
                    allDone.WaitOne();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();

        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.  
            allDone.Set();

            // Get the socket that handles the client request.  
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            // Create the state object.  
            SocketObject state = new SocketObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, SocketObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }

        public static void ReadCallback(IAsyncResult ar)
        {
            string content = string.Empty;
            string serializedResult = string.Empty;

            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            SocketObject state = (SocketObject)ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from the client socket.
            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.  
                state.sb.Append(Encoding.ASCII.GetString(
                    state.buffer, 0, bytesRead));

                // Check for end-of-file tag. If it is not there, read
                // more data.  
                content = state.sb.ToString();
                if (content.IndexOf("<EOF>") > -1)
                {
                    // All the data has been read from the
                    // client. Display it on the console.  
                    content = content.Replace("<EOF>", string.Empty);
                    ITDSSerializer serializer = SerializationFactory.GetSerializer(SerializerType.JSON);
                    //content = content.Replace("<EOF>", string.Empty);
              
                    Console.WriteLine("Read {0} bytes from socket. \n Data : {1}", content.Length, content);
                    //Deserialize conent into TDSRequest
                    TaskModel task = serializer.DeSerializeTaskModel(content);
                    //Execute task


                    //Dummy response for now
                    //TDSResponse response = new TDSResponse();
                    //response.protocolType = "response";
                    //response.SetHeader("protocolFormat", "json");
                    //response.SetHeader("protocolVersion", "1.0");
                    //response.sourceIP = "192.158.1.38";
                    //response.destIp = "192.0.2.1";
                    //response.sourcePort = 8001;
                    //response.destPort = 8080;
                    //response.setValue("status", "SUCCESS");
                    //response.setValue("node-id", "16");
                    //response.setValue("error-code", "202");
                    //response.setValue("error-message", "operation successful");

                    string fileType = Path.GetExtension(task.TaskPath);
                    ITaskExecutor executor = ExecutionFactory.GetTaskExector(fileType);

                    TDSResult result = executor.ExecuteTask(task);
                    if (result != null)
                    {
                        serializedResult = serializer.SerializeTDSResult(result);
                    }

                    Send(handler, serializedResult);
                }
                else
                {
                    // Not all data received. Get more.  
                    handler.BeginReceive(state.buffer, 0, SocketObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                }
            }
        }

        private static void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}

