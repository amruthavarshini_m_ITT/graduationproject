﻿using NUnit.Framework;
using System.Data;
using TaskDistributionSystem;
using TaskDistributionSystem.Data;
using TaskDistributionSystem.Model;

namespace Test.TaskDistribution
{
    class TestTask
    {
        TaskDAO taskDao = new TaskDAO();

        [Test]
        public void InsertTask()
        {

            var task = new TaskModel()
            {
                TaskID = 14,
                TaskParameters = "qwert@123",
                TaskPath = "1234.7865",
                TaskResult = "something",
                TaskStatusID = 2,
                ClientID = 1

            };

            taskDao.AddTask(task);

            Assert.Pass();
        }

        [Test]
        public void DeleteTask()
        {

            var task = new TaskModel()
            {
                TaskID = 14,

            };

            taskDao.DeleteTask(task);

            Assert.Pass();
        }

        [Test]
        public void GetTaskByTaskID()
        {
            var task = new TaskModel()
            {
                TaskID = 14,

            };

            taskDao.GetTaskByTaskID(task);

            Assert.Pass();
        }

        [Test]
        public void GetTaskByTaskStatus()
        {
            var task = new TaskModel()
            {
                 TaskStatusID = 14,

            };

            taskDao.GetTaskByTaskID(task);

            Assert.Pass();
        }


    }
}
