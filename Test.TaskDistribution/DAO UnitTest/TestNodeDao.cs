﻿using NUnit.Framework;
using System.Data;
using TaskDistributionSystem;
using TaskDistributionSystem.Data;
using TaskDistributionSystem.Model;

namespace Test.TaskDistribution
{
    class TestNode
    {
        NodeDAO nodeDao = new NodeDAO();

        [Test]
        public void InsertNode()
        {

            var node = new NodeModel()
            {
                Port = 14,
                HostName = "qwert@123",
                IPAddress = "1234.7865",
                NodeStatus = 1,
                TaskID = 1

            };

            nodeDao.AddNode(node);

            Assert.Pass();
        }

        [Test]
        public void DeleteNode()
        {

            var node = new NodeModel()
            {
                NodeID = 14,

            };

            nodeDao.DeleteNode(node);

            Assert.Pass();
        }

        [Test]
        public void GetAllNode()
        {
            nodeDao.GetAllNode();

            Assert.Pass();
        }

        [Test]
        public void GetAvailableNodes()
        {
            nodeDao.GetAvailableNodes();

            Assert.Pass();
        }


    }
}
