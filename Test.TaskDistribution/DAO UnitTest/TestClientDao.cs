using NUnit.Framework;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TaskDistributionSystem;
using TaskDistributionSystem.Data;
using TaskDistributionSystem.Model;

namespace Test.TaskDistribution
{
    public class TestClient
    {
            ClientDAO dao = new ClientDAO();

            [Test]
            public void InsertClient()
            {

                var client = new ClientModel()
                {
                    
                    HostName = "MYPC@123",
                    ClientAddress = "1234.7865",
                    Port = 3

                };

                dao.AddClient(client);

                Assert.Pass();
            }

        [Test]
        public void DeleteClient()
        {

            var client = new ClientModel()
            {
                ClientID = 5,

            };

            dao.DeleteClient(client);

            Assert.Pass();
        }

        [Test]
        public void GetClient()
        {

            var client = new ClientModel()
            {
                ClientID = 1,

            };

            List<ClientModel> clientdata = dao.GetClient(client);
            // ClientModel data = clientdata.ToArray<>();

            //Assert.AreEqual(client.ClientID, clientdata.);
            Assert.Pass();
        }
    }
}