﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TaskDistributionSystem.CommunicationProtocol;

namespace Test.TaskDistribution
{
    class TestSerialization
    {
        TDSRequest request = new TDSRequest();
        TDSSerializer serializer = new TDSSerializer();

        [Test]
        public void Request_Object_Serialization_Deserialization()
        {
            request.protocolType = "request";
            request.SetHeader("protocolFormat", "json");
            request.SetHeader("protocolVersion","1.0");
            request.sourceIP = "192.158.1.38";
            request.destIp = "192.0.2.1";
            request.destPort = 1003;
            request.sourcePort = 2099;
            request.setMethod("node-add");
            request.addParameter("node-IP", "192.153.2.1");
            request.addParameter("node-port", "20");

            //ACT
            string serializedData = serializer.Serialize(request);
            TDSProtocol deserializedData = serializer.DeSerialize(serializedData);

            //Assert
            deserializedData.headers.TryGetValue("protocolFormat", out string protocolFormat);
            Assert.AreEqual(protocolFormat, "json"); 
            deserializedData.payload.TryGetValue("method", out string method);
            Assert.AreEqual(method, "node-add");
        }

        [Test]
        public void Response_Object_Serialization_Deserialization()
        {
            TDSResponse response = new TDSResponse();

            response.setValue("node-id", "12");
            

            //ACT
            string serializedData = serializer.Serialize(response);
            TDSProtocol deserializedData = serializer.DeSerialize(serializedData);

            //Assert
            deserializedData.headers.TryGetValue("node-id", out string NodeID);
            Assert.AreEqual(NodeID, "12");
           
        }

    }
}
