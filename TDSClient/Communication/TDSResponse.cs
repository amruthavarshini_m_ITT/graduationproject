﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDS_Client_App.Communication
{
    public class TDSResponse : TDSProtocol
	{
		public TDSResponse()
		{
			protocolType = "response";
			protocolFormat = "json";
			protocolVersion = "1.0";
		}
		//Should return true/false, corresponds to status tag in the response
		public string getStatus()
        {
			headers.TryGetValue("status", out string status);
			return status;
        }
		public int getErrorCode()
        {
			headers.TryGetValue("error-code", out string errorCode);
			return int.Parse(errorCode);
        }
		public string getErrorMessage()
        {
			headers.TryGetValue("error-message", out string errorMessage);
			return errorMessage;
        }

		//Should return the value for a specific key from the response data, for example to retrieve the node-id
		public String getValue(String key)
		{
			headers.TryGetValue(key, out string value);
			return value;
		}
		public void setValue(String key, String value)
        {
			headers.TryAdd(key, value);
        }

	}
}
