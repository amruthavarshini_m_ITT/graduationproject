﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDS_Client_App.Communication
{
    interface ITDSSerializer
    {
        //DeSerialize the String data into a TDSProtocol object
        dynamic DeSerialize(String data);

        //Serialize the TDSProtocol object to a String format
        String Serialize(TDSProtocol protocol);
    }
}
