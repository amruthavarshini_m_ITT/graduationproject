﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDS_Client_App.Communication
{
    public class TDSSerializer : ITDSSerializer
    {
        public dynamic DeSerialize(string data)
        {
            JToken deSerializedData = JToken.Parse(data);
            string protocolType = deSerializedData["protocolType"].ToString();
            
            switch (protocolType)
            {
                case "request":
                   return  JsonConvert.DeserializeObject<TDSRequest>(data);

                case "response":
                    return JsonConvert.DeserializeObject<TDSResponse>(data);

                default:
                    return null;
            }    

        }

        public string Serialize(TDSProtocol protocol)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(protocol);
            return json;
        }
    }
}
