﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using ClientCommunicationLayer;
using TDS_Client_App.Communication;
using System.IO;

// State object for receiving data from remote device.  

namespace ClientProgram
{

    public static class ClientProgramme
    {
        public static int Main(String[] args)
        {


            var request = new TDSRequest()
            {
                sourceIP = "192.0.2.1",
                destIp = "192.158.1.38",
                sourcePort = 8080,
                destPort = 8001,
            };

            //request.setMethod("node-add");
            //request.addParameter("node-name", "node1");
            //request.addParameter("node-ip", "192.150.33.1");


            //Task setters
            string taskPath = "C:\\Users\\Amruthavarshini.m\\source\\repos\\MyProject\\Program.cs";
            request.setMethod("task-add");
            request.addParameter("task-path", taskPath);
            string base64EncodedFile = GetBase64EncodedFile(taskPath);

            request.addParameter("task-executable", base64EncodedFile);


            ClientCommunicationLayer.TDSClient client = new TDSClient();
            TDSResponse response = client.SendRequest(request);
            
            Console.WriteLine("Resposne status:" + response.getStatus());
            Console.WriteLine("Response error code:" + response.getErrorCode());
            Console.WriteLine("Response error message:" + response.getErrorMessage());
            Console.WriteLine("Task status:" + response.getValue("task-status"));
            Console.WriteLine("Task GUID:" + response.getValue("task-guid"));
            return 0;
        }

        private static string GetBase64EncodedFile(string taskPath)
        {
            byte[] bytes = File.ReadAllBytes(taskPath);
            string executable = Convert.ToBase64String(bytes);

            return executable;
        }
    }
}