﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDistributionSystem.CommunicationProtocol;

namespace TaskDistributionSystem.TDSCoordinator.Controllers
{
    public interface ITDSController
    {
        TDSResponse ProcessRequest(TDSRequest request);
    }
}
