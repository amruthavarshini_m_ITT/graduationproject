﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskDistributionSystem.CommunicationProtocol;
using TaskDistributionSystem.Core.Enums;
using TaskDistributionSystem.Data;
using TaskDistributionSystem.Model;
using TaskDistributionSystem.Queue;

namespace TaskDistributionSystem.TDSCoordinator.Controllers
{
    public class TaskController : ITDSController
    {
        private readonly TaskDAO _taskDAO;
        public TaskController()
        {
            _taskDAO = new TaskDAO();
        }

        public TDSResponse ProcessRequest(TDSRequest request)
        {
            TDSResponse response = new TDSResponse();
            if (request.GetMethod() == "task-add")
            {
                ProcessTask(request, response);
            }
            //else if - task-status
            //else if - task-result
            return response;
        }

        private void ProcessTask(TDSRequest request, TDSResponse response)
        {
            try
            {
                TaskModel task = new TaskModel()
                {
                    TaskPath = request.getParameter("task-path"),
                    TaskGuid = Guid.NewGuid().ToString()
                };
                _taskDAO.AddTask(task);
                task.TaskExecutable = request.getParameter("task-executable");
                // Queue the task
                TDSQueue.queue.Enqueue(task);

                // Update db
                _taskDAO.SetTaskStatus(TaskStatus.QUEUED, task.TaskGuid);

                SetResponse(response, "SUCCESS", task.TaskGuid, "200", "Operation Successful");
            }
            catch (Exception exception)
            {
                SetResponse(response, "FAILURE", string.Empty, "500", exception.Message);
            }
        }

        private void SetResponse(TDSResponse response, string status, string taskGuid, string errorCode, string errorMessage)
        {
            response.setValue("status", status);
            response.setValue("task-guid", taskGuid);
            response.setValue("error-code", errorCode);
            response.setValue("error-message", errorMessage);
        }
    }
}

