﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDistributionSystem.CommunicationProtocol;
using TaskDistributionSystem.Core.Enums;
using TaskDistributionSystem.Data;
using TaskDistributionSystem.Model;

namespace TaskDistributionSystem.TDSCoordinator.Controllers
{
    public class NodeController : ITDSController
    {
       private readonly NodeDAO _nodeDAO;
        public NodeController()
        {
            _nodeDAO = new NodeDAO();
        }

        public TDSResponse ProcessRequest(TDSRequest request)
        {
            TDSResponse response = new TDSResponse();
            if (request.GetMethod() == "node-add")
            {
                RegisterNode(request, response);
            }
            //else if - task-status
            //else if - task-result
            return response;
        }

        private void RegisterNode(TDSRequest request, TDSResponse response)
        {
            try
            {
                NodeModel node = new NodeModel()
                {
                    NodeGuid = Guid.NewGuid().ToString(),
                    NodeStatus = (int)NodeStatus.FREE,
                    HostName = request.getParameter("node-name"),
                    Port = 12000,
                    IPAddress = request.getParameter("node-ip"),
                };
                _nodeDAO.AddNode(node);
               
                // Update db
                //_nodeDAO.SetTaskStatus(TaskStatus.QUEUED, task.TaskGuid);

                SetResponse(response, "SUCCESS", node.NodeGuid, "200", "Operation Successful");
            }
            catch (Exception exception)
            {
                SetResponse(response, "FAILURE", string.Empty, "500", exception.Message);
            }
        }

        private void SetResponse(TDSResponse response, string status, string nodeGuid, string errorCode, string errorMessage)
        {
            response.setValue("status", status);
            response.setValue("node-guid", nodeGuid);
            response.setValue("error-code", errorCode);
            response.setValue("error-message", errorMessage);
        }
    }
    
}
