﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using TaskDistributionSystem.Data;
using TaskDistributionSystem.Interface;
using TaskDistributionSystem.Model;


namespace TaskDistributionSystem
{
    public class Client// : IClientDAO
    {
      

        ClientModel clientData = new ClientModel();
        ClientDAO dao = new ClientDAO();

        public void AddClient(ClientModel clientData)
        {

            dao.AddClient(clientData);
        }

        public void DeleteClient(ClientModel clientData)
        {
            dao.DeleteClient(clientData);
        }

        //public DataTable GetClient(ClientModel clientData)
        //{
        //    DataTable dt = dao.GetClient(clientData);
        //    return dt;
        //}

        public void ModifyClient(ClientModel clientData)
        {
            dao.ModifyClient(clientData);
        }

       
    }
}
