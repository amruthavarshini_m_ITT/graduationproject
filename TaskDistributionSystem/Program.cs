﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using TaskDistributionSystem.TaskDispatchment;
using TaskDistributionSystem.TDSCommunicationLayer;

// State object for reading client data asynchronously  

namespace TDSServerProgram
{
    public static class ServerProgram
    {

        public static int Main(String[] args)
        {
            TaskDispatcher dispatcher = new TaskDispatcher();
            Thread dispatcherThread = new Thread(dispatcher.DispatchTasks);
            dispatcherThread.Start();

            TDSServer server = new TDSServer();

            server.StartListening();
            return 0;
        }
    }
}
