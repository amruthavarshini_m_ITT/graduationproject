﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDistributionSystem.Data;
using TaskDistributionSystem.Interface;
using TaskDistributionSystem.Model;

namespace TaskDistributionSystem
{
    class Task //: ITaskDAO
    {
        TaskModel taskData = new TaskModel();
        TaskDAO dao = new TaskDAO();

        public void AddTask(TaskModel taskData)
        {
            dao.AddTask(taskData);
        }

        public void AssignNode()
        {
            throw new NotImplementedException();
        }

        public void DeleteTask(TaskModel taskData)
        {
            dao.DeleteTask(taskData);
        }

        //public DataTable GetTaskByTaskID(TaskModel taskData)
        //{
        //    DataTable dt = dao.GetTaskByTaskID(taskData);
        //    foreach (DataRow dataRow in dt.Rows)
        //    {
        //        foreach (var item in dataRow.ItemArray)
        //        {
        //            Console.WriteLine(item);
        //        }
        //    }
        //    return dt;
        //}

        //public DataTable GetTaskByTaskStatus(TaskModel taskData)
        //{
        //    DataTable dt = dao.GetTaskByTaskStatus(taskData);
        //    foreach (DataRow dataRow in dt.Rows)
        //    {
        //        foreach (var item in dataRow.ItemArray)
        //        {
        //            Console.WriteLine(item);
        //        }
        //    }
        //    return dt;
        //}

        public void ModifyTask(TaskModel taskData)
        {
            dao.ModifyTask(taskData);
        }
    }
}
