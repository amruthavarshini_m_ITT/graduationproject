﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDistributionSystem.Model;

namespace TaskDistributionSystem.CommunicationProtocol
{
    interface ITDSSerializer
    {
        //DeSerialize the String data into a TDSProtocol object
        dynamic DeSerialize(String data);

        TaskModel DeSerializeTaskModel(string data);

        TDSResult DeSerializeTDSResult(string data);

        //Serialize the TDSProtocol object to a String format
        String Serialize(TDSProtocol protocol);

        string SerializeTask(TaskModel task);
    }
}
