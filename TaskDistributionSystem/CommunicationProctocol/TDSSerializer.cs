﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDistributionSystem.Model;

namespace TaskDistributionSystem.CommunicationProtocol
{
    public class TDSSerializer : ITDSSerializer
    {
        public dynamic DeSerialize(string data)
        {
            JToken deSerializedData = JToken.Parse(data);
            string protocolType = deSerializedData["protocolType"].ToString();
            
            switch (protocolType)
            {
                case "request":
                   return JsonConvert.DeserializeObject<TDSRequest>(data);

                case "response":
                    return JsonConvert.DeserializeObject<TDSResponse>(data);

                default:
                    return null;
            }    

        }

        public TaskModel DeSerializeTaskModel(string data)
        {
            return JsonConvert.DeserializeObject<TaskModel>(data);
        }

        public TDSResult DeSerializeTDSResult(string data)
        {
            return JsonConvert.DeserializeObject<TDSResult>(data);
        }

        public string Serialize(TDSProtocol protocol)
        {
            string json = JsonConvert.SerializeObject(protocol);
            return json;
        }

        public string SerializeTask(TaskModel task)
        {
            string json = JsonConvert.SerializeObject(task);
            return json;
        }
    }
}
