﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDistributionSystem.CommunicationProtocol.Enums;

namespace TaskDistributionSystem.CommunicationProtocol
{
    class SerializationFactory
    {
        public static ITDSSerializer GetSerializer(SerializerType type)
        {
           switch (type)
            {
                case SerializerType.JSON:
                    return new TDSSerializer();

                case SerializerType.XML:
                    return new TDSSerializer();

                default:
                    throw new InvalidOperationException("Invalid Serializer type");
            }
        }
    }
}
