﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskDistributionSystem.CommunicationProtocol
{
	public class TDSProtocol
	{
		public TDSProtocol() { }

		public String sourceIP { get; set; }
		public String destIp { get; set; }
		public int sourcePort { get; set; }
		public int destPort { get; set; }
		//Defines the version of protocol and maps to "TDSProtocolVersion" attribute in the protocol
		public String protocolVersion { get; set; }
		//Defines the format of protocol and maps to "TDSProtocolFormat" attribute in the protocol
		public String protocolFormat { get; set; }
		//Corresponds to "type" attribute in protocol definition, defines the type of request, could be a request
		// or a response type.
		public String protocolType { get; set; }
		//Contains the key value pairs as specified in protocol "header" object.
		public ConcurrentDictionary<String, String> headers = new ConcurrentDictionary<string, string>();

		public ConcurrentDictionary<String, String> payload = new ConcurrentDictionary<string, string>();

		
		public String GetHeader(String headerKey)
        {
			headers.TryGetValue(headerKey, out string headerValue);
			return headerValue;
        }
		
		public void SetHeader(String headerKey,String value)
        {
			headers.TryAdd(headerKey, value);
		
        }

	}
}
