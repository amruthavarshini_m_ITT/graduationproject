﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskDistributionSystem.CommunicationProtocol
{
    public class TDSRequest : TDSProtocol
    {
		public TDSRequest()
		{
			protocolType = "request";
			protocolFormat = "json";
			protocolVersion = "1.0";
		}
		//corresponds to method in the header
		public String GetMethod()
        {
			payload.TryGetValue("method", out string method);
			return method;
        }
		public void setMethod(String method)
        {
			payload.TryAdd("method", method);
			
		}

		//Get's the value of a given parameter with specified key
		public String getParameter(String key)
        {
			payload.TryGetValue(key, out string parameter);
			return parameter;
        }

		//Adds a parameter for the request
		public void addParameter(String key, String value)
        {
			payload.TryAdd(key, value);
		
        }
	}
}
