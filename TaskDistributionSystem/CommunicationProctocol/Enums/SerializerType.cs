﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskDistributionSystem.CommunicationProtocol.Enums
{
    public enum SerializerType
    {
        JSON,
        XML
    }
}

