﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDistributionSystem.CommunicationProtocol;
using TaskDistributionSystem.TDSCoordinator.Controllers;

namespace TaskDistributionSystem.RequestDispatchment
{
    public class RequestDispatcher
    {
        public static ITDSController GetTDSController(TDSRequest request)
        {
            if (request.GetMethod().StartsWith("task-"))
            {
                return new TaskController();
            }
            else if (request.GetMethod().StartsWith("node-"))
            {
                return new NodeController();
            }
            else if (request.GetMethod().StartsWith("client-"))
            {
                return new ClientController();
            }
            else
                return null;
        }
    }
}
