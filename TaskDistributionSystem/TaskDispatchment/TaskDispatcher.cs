﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDistributionSystem.Data;
using TaskDistributionSystem.Model;
using TaskDistributionSystem.Queue;
using TaskDistributionSystem.TDSCommunicationLayer;

namespace TaskDistributionSystem.TaskDispatchment
{
    public class TaskDispatcher
    {
        private readonly NodeDAO _nodeDAO;
        private readonly TaskDAO _taskDAO;
        public TaskDispatcher()
        {
            _nodeDAO = new NodeDAO();
            _taskDAO = new TaskDAO();
        }

        public void DispatchTasks()
        {
            Console.WriteLine("Waiting for Tasks to arrive in queue");

            while (true)
            {
                if (TDSQueue.queue.TryDequeue(out TaskModel task))
                {
                    List<NodeModel> availableNodes = _nodeDAO.GetAvailableNodes();
                    
                    if(availableNodes.Count == 0)
                    {
                        TDSQueue.queue.Enqueue(task);
                        continue;
                    }

                    NodeModel node = availableNodes[0];
                    string ipAddress = node.IPAddress;
                    int portNumber = node.Port;

                    NodeClient client = new NodeClient(ipAddress, portNumber);
                    // Make the node busy
                    TDSResult result = client.SendTask(task);

                    if (result != null)
                    {
                        SetTaskResult(result, task);
                        //Make the node available again
                    }
                }
            }
        }

        private void SetTaskResult(TDSResult result, TaskModel task)
        {
            _taskDAO.SetTaskStatus(result.TaskStatus, task.TaskGuid);
            _taskDAO.SetResult(result.TaskOutcome, task.TaskGuid);
        }
    }
}
