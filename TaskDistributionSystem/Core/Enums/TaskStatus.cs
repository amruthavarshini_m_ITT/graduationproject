﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskDistributionSystem.Core.Enums
{
    public enum TaskStatus
    {
        PENDING,
        QUEUED,
        EXECUTING,
        COMPLETED,
        ABORTED
    }
}
