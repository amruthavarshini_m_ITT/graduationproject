﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskDistributionSystem.Core.Enums
{
    public enum NodeStatus
    {
        FREE,
        BUSY,
        UNAVAILABLE
    }
}
