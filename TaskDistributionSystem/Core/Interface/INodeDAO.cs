﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using TaskDistributionSystem.Model;
using System.Data.SqlClient;

namespace TaskDistributionSystem.Interface
{
    interface INodeDAO
    {
        
        void AddNode(NodeModel nodeData);
        void ModifyNode(NodeModel nodeData);
        void DeleteNode(NodeModel nodeData);
        List<NodeModel> GetAvailableNodes();
        List<NodeModel> GetAllNode();
    }
}