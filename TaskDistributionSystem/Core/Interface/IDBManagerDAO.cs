﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace TaskDistributionSystem.Interface
{
    public interface IDBManagerDAO
    {
        void CloseConnection(SqlConnection connection);
        void ExecuteNonQuery(SqlCommand command);
        SqlDataReader ExecuteSelectQuery(SqlCommand command);

    }
}
