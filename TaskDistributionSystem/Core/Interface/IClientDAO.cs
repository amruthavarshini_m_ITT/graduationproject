﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using TaskDistributionSystem.Model;
using System.Collections;
using System.Data.SqlClient;

namespace TaskDistributionSystem.Interface
{
    interface IClientDAO
    {
        void AddClient(ClientModel clientData);
        void ModifyClient(ClientModel clientData);
        void DeleteClient(ClientModel clientData);
        List<ClientModel> GetClient(ClientModel clientData);
    }
}
