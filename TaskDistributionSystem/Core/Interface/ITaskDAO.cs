﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDistributionSystem.Model;

namespace TaskDistributionSystem.Interface
{
    interface ITaskDAO
    {
        void AddTask(TaskModel taskData);
        void ModifyTask(TaskModel taskData);
        void DeleteTask(TaskModel taskData);
        List<TaskModel> GetTaskByTaskID(TaskModel taskData);
        List<TaskModel> GetTaskByTaskStatus(string TaskStatus);
        void AssignNode(TaskModel taskData, NodeModel nodedata);
    }
}
