﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TaskDistributionSystem.Core.Enums;
using TaskDistributionSystem.Interface;
using TaskDistributionSystem.Model;

namespace TaskDistributionSystem.Data
{
    public class TaskDAO : ITaskDAO
    {
        string connectionString = @"Data Source=(local);Initial Catalog=TDS;Integrated Security = True";
        SqlConnection connection = null;

        DBManager dBManager = new DBManager();

        public void AddTask(TaskModel taskData)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("AddTask", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@TaskPath", SqlDbType.VarChar).Value = taskData.TaskPath;
                    command.Parameters.AddWithValue("@TaskGuid", SqlDbType.VarChar).Value = taskData.TaskGuid;



                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public void SetResult(string taskOutcome, string taskGuid)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("SetTaskResult", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@TaskGuid", SqlDbType.NChar).Value = taskGuid;
                    command.Parameters.AddWithValue("@TaskResult", SqlDbType.VarChar).Value = taskOutcome;
                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public void AssignNode(TaskModel taskData, NodeModel nodeData)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("Assignnode", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@TaskID", SqlDbType.Int).Value = taskData.TaskID;
                    command.Parameters.AddWithValue("@NodeID", SqlDbType.Int).Value = nodeData.NodeID;



                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }
    

        public void DeleteTask(TaskModel taskData)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("DeleteTask", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@TaskID", SqlDbType.Int).Value = taskData.TaskID;

                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public List<TaskModel> GetTaskByTaskID(TaskModel taskData)
        {
            using (connection = new SqlConnection(connectionString))
            {
                List<TaskModel> tasks = new List<TaskModel>();
                connection.Open();
                Console.WriteLine("Open connection Successfull");
                SqlCommand command = new SqlCommand("GetTaskByTaskID", connection);
                command.Parameters.AddWithValue("@TaskID", SqlDbType.Int).Value = taskData.TaskID;
                SqlDataReader reader = dBManager.ExecuteSelectQuery(command);
                while (reader.Read())
                {
                    tasks.Add(new TaskModel
                    {
                        TaskID = Convert.ToInt32(reader["TaskID"]),
                        TaskParameters = reader["TaskParameters"].ToString(),
                        TaskPath = reader["TaskPath"].ToString(),
                        TaskResult = reader["TaskResult"].ToString(),
                        TaskStatusID = Convert.ToInt32(reader["TaskStatusID"]),
                        ClientID = Convert.ToInt32(reader["ClientID"]),
                        TaskStatus = Convert.ToInt32(reader["TaskStatus"]),
                        TaskGuid = reader["TaskGuid"].ToString(),


                    });


                }
                dBManager.CloseConnection(connection);
                return tasks;
            }
        }

        public List<TaskModel> GetTaskByTaskStatus(string TaskStatus)
        {
            List<TaskModel> tasks = new List<TaskModel>();
            connection.Open();
            Console.WriteLine("Open connection Successfull");
            SqlCommand command = new SqlCommand("GetTaskByTaskStatus", connection);
            command.Parameters.AddWithValue("@TaskStatusID", SqlDbType.Int).Value = TaskStatus;
            SqlDataReader reader = dBManager.ExecuteSelectQuery(command);
            while (reader.Read())
            {
                tasks.Add(new TaskModel
                {
                    TaskID = Convert.ToInt32(reader["TaskID"]),
                    TaskParameters = reader["TaskParameters"].ToString(),
                    TaskPath = reader["TaskPath"].ToString(),
                    TaskResult = reader["TaskResult"].ToString(),
                    TaskStatusID = Convert.ToInt32(reader["TaskStatusID"]),
                    ClientID = Convert.ToInt32(reader["ClientID"]),
                    TaskStatus = Convert.ToInt32(reader["TaskStatus"]),
                    TaskGuid = reader["TaskGuid"].ToString(),

                });


            }
            dBManager.CloseConnection(connection);
            return tasks;
        }

        public void ModifyTask(TaskModel taskData)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("UpdateTask", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@TaskID", SqlDbType.Int).Value = taskData.TaskID;
                    command.Parameters.AddWithValue("@TaskStatusID", SqlDbType.Int).Value = taskData.TaskStatusID;
                    command.Parameters.AddWithValue("@TaskPath", SqlDbType.VarChar).Value = taskData.TaskPath;
                    command.Parameters.AddWithValue("@TaskResult", SqlDbType.VarChar).Value = taskData.TaskResult;
                    command.Parameters.AddWithValue("@TaskParameters", SqlDbType.VarChar).Value = taskData.TaskParameters;
                    command.Parameters.AddWithValue("@ClientID", SqlDbType.Int).Value = taskData.ClientID;

                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public void SetTaskStatus(TaskStatus status, string taskGuid)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("UpdateTask", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@TaskGuid", SqlDbType.NChar).Value = taskGuid;
                    command.Parameters.AddWithValue("@TaskStatus", SqlDbType.Int).Value = (int)status;
                    
                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }
    }
}
