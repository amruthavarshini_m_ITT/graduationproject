﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.Threading.Tasks;
using TaskDistributionSystem.Interface;

namespace TaskDistributionSystem.Data
{
    class DBManager : IDBManagerDAO
    {
        public void CloseConnection(SqlConnection connection)
        {
            connection.Close();
        }

        public void ExecuteNonQuery(SqlCommand command)
        {
        
            command.ExecuteNonQuery();
        }

        public SqlDataReader ExecuteSelectQuery(SqlCommand command)
        {
            DataTable dt = new DataTable();

            command.CommandType = CommandType.StoredProcedure;
            //SqlDataAdapter da = new SqlDataAdapter(command);
            //dt = new DataTable();
            //da.Fill(dt);
            //return dt;
            SqlDataReader reader = command.ExecuteReader();
            return reader;
        }
    }
}
