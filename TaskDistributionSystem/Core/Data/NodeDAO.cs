﻿using System;
using TaskDistributionSystem.Interface;
using System.Data.SqlClient;
using System.Data;
using TaskDistributionSystem.Model;
using TaskDistributionSystem.Data;
using System.Configuration;
using System.Collections.Generic;

namespace TaskDistributionSystem.Data
{
    public class NodeDAO : INodeDAO
    {
        string connectionString = @"Data Source=(local);Initial Catalog=TDS;Integrated Security = True";
        SqlConnection connection = null;

        DBManager dBManager = new DBManager();

        public void AddNode(NodeModel nodeData)
        {
            

            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("AddNode", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@HostName", SqlDbType.VarChar).Value = nodeData.HostName;
                    command.Parameters.AddWithValue("@Address", SqlDbType.VarChar).Value = nodeData.IPAddress;
                    command.Parameters.AddWithValue("@Port", SqlDbType.Int).Value = nodeData.Port;
                    command.Parameters.AddWithValue("@NodeStatus", SqlDbType.Int).Value = nodeData.NodeStatus;
                    command.Parameters.AddWithValue("@NodeGuid", SqlDbType.Int).Value = nodeData.NodeGuid;

                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public void ModifyNode(NodeModel nodeData)
        {

            try
            {

                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("UpdateNode", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@NodeID", SqlDbType.Int).Value = nodeData.NodeID;
                    command.Parameters.AddWithValue("@HostName", SqlDbType.VarChar).Value = nodeData.HostName;
                    command.Parameters.AddWithValue("@Address", SqlDbType.VarChar).Value = nodeData.IPAddress;
                    command.Parameters.AddWithValue("@Port", SqlDbType.Int).Value = nodeData.IPAddress;
                   

                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public void DeleteNode(NodeModel nodeData)
        {

            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("DeleteNode", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@NodeID", SqlDbType.Int).Value = nodeData.NodeID;
                    
                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public List<NodeModel> GetAvailableNodes()
        {
            List<NodeModel> nodeList = new List<NodeModel>();
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                   

                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("GetAvailableNode", connection);
                    SqlDataReader reader = dBManager.ExecuteSelectQuery(command);
                    while (reader.Read())
                    {
                        nodeList.Add(new NodeModel
                        {
                            NodeID = Convert.ToInt32(reader["NodeID"]),
                            HostName = reader["HostName"].ToString(),
                            IPAddress = reader["NodeAddress"].ToString(),
                            Port = Convert.ToInt32(reader["Port"]),
                            NodeStatus = Convert.ToInt32(reader["NodeStatus"]),
                            TaskID = Convert.ToInt32(reader["TaskID"]),

                        });


                    }
                    dBManager.CloseConnection(connection);
                    
                }
            
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return nodeList;
        }

        public List<NodeModel> GetAllNode()
        {

            using (connection = new SqlConnection(connectionString))
            {
                List<NodeModel> node = new List<NodeModel>();

                connection.Open();
                Console.WriteLine("Open connection Successfull");
                SqlCommand command = new SqlCommand("GetAllNode", connection);
                SqlDataReader reader = dBManager.ExecuteSelectQuery(command);
                while (reader.Read())
                {
                    node.Add(new NodeModel
                    {
                        NodeID = Convert.ToInt32(reader["NodeID"]),
                        HostName = reader["HostName"].ToString(),
                        IPAddress = reader["NodeAddress"].ToString(),
                        NodeStatus = Convert.ToInt32(reader["NodeStatus"]),
                        TaskID = Convert.ToInt32(reader["TaskID"]),

                    });

                }
                dBManager.CloseConnection(connection);
                return node;
            }
        }
    }

}
