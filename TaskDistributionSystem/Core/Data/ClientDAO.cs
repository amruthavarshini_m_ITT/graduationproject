﻿using System;
using System.Data.SqlClient;
using System.Data;
using TaskDistributionSystem.Model;
using TaskDistributionSystem.Data;
using System.Collections.Generic;
using TaskDistributionSystem.Interface;

namespace TaskDistributionSystem.Data
{
    public class ClientDAO : IClientDAO
    {
        string connectionString = @"Data Source=(local);Initial Catalog=TDS;Integrated Security = True";
        SqlConnection connection = null;

        DBManager dBManager = new DBManager();

        public void AddClient(ClientModel clientData)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("AddClient", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ClientID", SqlDbType.Int).Value = clientData.ClientID;
                    command.Parameters.AddWithValue("@HostName", SqlDbType.VarChar).Value = clientData.HostName;
                    command.Parameters.AddWithValue("@ClientAddress", SqlDbType.Int).Value = clientData.ClientAddress;
                    command.Parameters.AddWithValue("@Port", SqlDbType.VarChar).Value = clientData.Port;

                    dBManager.ExecuteNonQuery(command);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            finally
            {
                dBManager.CloseConnection(connection);
            }
        }

        public void ModifyClient(ClientModel clientData)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("UpdateClient", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ClientID", SqlDbType.Int).Value = clientData.ClientID;
                    command.Parameters.AddWithValue("@HostName", SqlDbType.VarChar).Value = clientData.HostName;
                    command.Parameters.AddWithValue("@ClientAddress", SqlDbType.Int).Value = clientData.ClientAddress;
                    command.Parameters.AddWithValue("@Port", SqlDbType.VarChar).Value = clientData.Port;

                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public void DeleteClient(ClientModel clientData)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Open connection Successfull");
                    SqlCommand command = new SqlCommand("DeleteClient", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ClientID", SqlDbType.Int).Value = clientData.ClientID;

                    dBManager.ExecuteNonQuery(command);
                    dBManager.CloseConnection(connection);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public List<ClientModel> GetClient(ClientModel clientData)
        {

            using (connection = new SqlConnection(connectionString))
            {
                List<ClientModel> client = new List<ClientModel>();

                connection.Open();
                Console.WriteLine("Open connection Successfull");
                SqlCommand command = new SqlCommand("GetAllClient", connection);
                command.Parameters.AddWithValue("@ClientID", SqlDbType.Int).Value = clientData.ClientID;
                SqlDataReader reader = dBManager.ExecuteSelectQuery(command);
                while (reader.Read())
                {
                    client.Add(new ClientModel
                    {
                        ClientID = Convert.ToInt32(reader["ClientID"]),
                        HostName = reader["HostName"].ToString(),
                        ClientAddress = reader["ClientAddress"].ToString(),
                        Port = Convert.ToInt32(reader["ClientID"]),

                    });
                   

                }
                dBManager.CloseConnection(connection);
                return client;
            }


        }
    }
}
