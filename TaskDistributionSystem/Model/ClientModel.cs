﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskDistributionSystem.Model
{
    public class ClientModel
    {
        public int ClientID { get; set; }
        public string HostName { get; set; }
        public string ClientAddress { get; set; }
        public int Port { get; set; }

    }
}
