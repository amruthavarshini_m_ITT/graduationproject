﻿using System;

namespace TaskDistributionSystem.Model
{
    public class NodeModel
    {
        public int NodeID { get; set; }
        public string HostName { get; set; }
        public string IPAddress { get; set; }
        public int NodeStatus { get; set; }
        public int TaskID { get; set; }
        public int Port { get; set; }

        public string NodeGuid { get; set; }

    }
}
