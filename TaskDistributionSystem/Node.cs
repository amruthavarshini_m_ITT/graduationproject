﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TaskDistributionSystem.Data;
using TaskDistributionSystem.Interface;
using TaskDistributionSystem.Model;
using System.Configuration;


namespace TaskDistributionSystem
{
    public class Node //: INodeDAO
    {
       
        NodeModel nodeData = new NodeModel();
        NodeDAO dao = new NodeDAO();

        public void AddNode(NodeModel nodeData)
        {
            nodeData.NodeID = 10;
            nodeData.HostName = "tgh";
            nodeData.NodeStatus = 2;
            nodeData.IPAddress = "1233445";
            nodeData.TaskID = 1;

            dao.AddNode(nodeData);
        }

        public void DeleteNode(NodeModel nodeData)
        {

            nodeData.NodeID = 1;

            dao.DeleteNode(nodeData);
        }

        //public DataTable GetAllNode()
        //{

        //    DataTable dt = dao.GetAvailableNode();
        //    foreach (DataRow dataRow in dt.Rows)
        //    {
        //        foreach (var item in dataRow.ItemArray)
        //        {
        //            Console.WriteLine(item);
        //        }
        //    }
        //    return dt;
        //}

        //public DataTable GetAvailableNode()
        //{

        //    DataTable dt = dao.GetAllNode();
        //    foreach (DataRow dataRow in dt.Rows)
        //    {
        //        foreach (var item in dataRow.ItemArray)
        //        {
        //            Console.WriteLine(item);
        //        }
        //    }
        //    return dt;

        //}

        public void ModifyNode(NodeModel nodeData)
        {

            dao.ModifyNode(nodeData);
        }
    }
}
